extends Node2D

func _ready():
	$Tetromino.connect("pausing_game", self, "_on_Tetromino_pausing_game")
	$Tetromino.connect("end_game", self, "_on_Tetromino_end_game")
	$GameOver/Overlay/Buttons/Replay.connect("button_up", self, "_on_GameOver_replay")
	$GameOver/Overlay/Buttons/Quit.connect("button_up", self, "_on_GameOver_quit")

func _on_Tetromino_pausing_game():
	$Pause.toggle_visibility(true)

func _on_Tetromino_end_game():
	$GameOver.toggle_visibility(true)

func _on_GameOver_replay():
	get_tree().reload_current_scene()

func _on_GameOver_quit():
	get_tree().quit()
