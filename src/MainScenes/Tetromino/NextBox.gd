extends TextureRect

func _on_Tetromino_new_next_piece(next_piece: int):
	var map = get_parent()
	for px in range(4):
		for py in range(4):
			if map.tetromino[next_piece][px + py * 4] == "X":
				map.set_cell(14 + px, 3 + py, next_piece)
			else:
				map.set_cell(14 + px, 3 + py, -1)
