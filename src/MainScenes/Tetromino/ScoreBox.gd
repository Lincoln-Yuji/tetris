extends TextureRect

func _on_Tetromino_score_calculated(new_score: int):
	var str_new_score = str(new_score)
	while str_new_score.length() < 8:
		str_new_score = "0" + str_new_score
	$ScoreText.text = "Score: " + str_new_score
