extends TextureRect

func _on_Tetromino_swapped_piece(holding_piece: int):
	var map = get_parent()
	for px in range(4):
		for py in range(4):
			if map.tetromino[holding_piece][px + py * 4] == "X":
				map.set_cell(-6 + px, 3 + py, holding_piece)
			else:
				map.set_cell(-6 + px, 3 + py, -1)
