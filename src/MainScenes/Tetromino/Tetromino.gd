extends TileMap

onready var forcing_down: Timer = $ForcingDown
var down_delay: float = 0.8
var force_down: bool = false

onready var translate_delay: Timer = $TranslateDelay
var delay: float = 0.1

const EMPTY_CELL: int = -1
const SPAWN_POSITION: Vector2 = Vector2(4, -2)

var rng: = RandomNumberGenerator.new()

var tetromino: Array = ["", "", "", "", "", "", ""]
var tab_size: Vector2 = Vector2(12, 19)

var current_piece: int = 0
var current_rot: int = 0
var current_pos: Vector2 = SPAWN_POSITION

var holding_piece: int = -1
var next_piece: int = 0

var lines_filled: Array = []
var score: int = 0

signal pausing_game
signal end_game
signal score_calculated
signal swapped_piece
signal new_next_piece

func _ready():
	self.connect("score_calculated", $ScoreBox, "_on_Tetromino_score_calculated")
	self.connect("swapped_piece", $HoldBox, "_on_Tetromino_swapped_piece")
	self.connect("new_next_piece", $NextBox, "_on_Tetromino_new_next_piece")

	tetromino[0] += ("....")
	tetromino[0] += ("XXXX")
	tetromino[0] += ("....")
	tetromino[0] += ("....")

	tetromino[1] += ("..X.")
	tetromino[1] += (".XX.")
	tetromino[1] += (".X..")
	tetromino[1] += ("....")

	tetromino[2] += (".X..")
	tetromino[2] += (".XX.")
	tetromino[2] += ("..X.")
	tetromino[2] += ("....")

	tetromino[3] += (".XX.")
	tetromino[3] += (".X..")
	tetromino[3] += (".X..")
	tetromino[3] += ("....")

	tetromino[4] += (".XX.")
	tetromino[4] += ("..X.")
	tetromino[4] += ("..X.")
	tetromino[4] += ("....")

	tetromino[5] += ("....")
	tetromino[5] += (".XX.")
	tetromino[5] += (".XX.")
	tetromino[5] += ("....")

	tetromino[6] += ("..X.")
	tetromino[6] += (".XX.")
	tetromino[6] += ("..X.")
	tetromino[6] += ("....")

	rng.randomize()
	next_piece = rng.randi_range(0, 6)
	forcing_down.start(down_delay)
	_set_new_piece()

func _process(_delta: float):
	if Input.is_action_just_pressed("Pause"):
		print("Helloo!!!")
		emit_signal("pausing_game")
		get_tree().paused = true
	if translate_delay.is_stopped():
		if forcing_down.is_stopped():
			force_down = true
			forcing_down.start(down_delay)
		if Input.is_action_pressed("ui_left") and _does_piece_fit(current_piece, current_rot, current_pos - Vector2(1, 0)):
			_erase_piece()
			current_pos -= Vector2(1, 0)
			translate_delay.start(delay)
		if Input.is_action_pressed("ui_right") and _does_piece_fit(current_piece, current_rot, current_pos + Vector2(1, 0)):
			_erase_piece()
			current_pos += Vector2(1, 0)
			translate_delay.start(delay)
		if Input.is_action_pressed("ui_down"):
			force_down = true
	if Input.is_action_just_pressed("Rotate") and _does_piece_fit(current_piece, current_rot + 1, current_pos):
		_erase_piece()
		current_rot += 1
		translate_delay.start(delay)
	if Input.is_action_just_pressed("Swap"):
		_erase_piece()
		if holding_piece == -1:
			holding_piece = current_piece
			_set_new_piece()
		else:
			var piece_handler = holding_piece
			holding_piece = current_piece
			current_piece = piece_handler
			for i in [0, -1, 1]:
				if _does_piece_fit(current_piece, current_rot, current_pos + Vector2(i, 0)):
					current_pos.x += i
					break
		emit_signal("swapped_piece", holding_piece)
	if force_down:
		force_down = false
		forcing_down.stop()
		forcing_down.start(down_delay)
		if _does_piece_fit(current_piece, current_rot, current_pos + Vector2(0, 1)):
			_erase_piece()
			current_pos += Vector2(0, 1)
			translate_delay.start(delay)
		else:
			_fix_current_piece()
			_clean_full_lines()
			_calculate_score()
			_take_tiles_down()
			_check_game_over()
			_set_new_piece()
	_draw_piece()

func _does_piece_fit(piece_id: int, rot: int, pos: Vector2) -> bool:
	for px in range(4):
		for py in range(4):
			var i: int = _rotate(px, py, rot)
			if tetromino[piece_id][i] == "X" and get_cell(px + pos.x, py + pos.y) in range(7, 15):
				return false
	return true

func _rotate(px: int, py: int, r: int) -> int:
	match r % 4:
		0:
			return px + 4 * py
		1:
			return 12 + py - 4 * px
		2:
			return 15 - px - 4 * py
		3:
			return 3 - py + 4 * px
	return 0

func _set_new_piece():
	rng.randomize()
	current_piece = next_piece
	current_rot = rng.randi_range(0, 3)
	current_pos = SPAWN_POSITION
	next_piece = rng.randi_range(0, 6)
	emit_signal("new_next_piece", next_piece)

func _fix_current_piece():
	for px in range(4):
		for py in range(4):
			if tetromino[current_piece][_rotate(px, py, current_rot)] == "X":
				set_cell(px + current_pos.x, py + current_pos.y, current_piece + 7)

func _clean_full_lines():
	for py in range(4):
		var line_full: bool = true
		for px in range(1, 11):
			var cell: = get_cell(px, py + current_pos.y)
			if cell == EMPTY_CELL or cell == 14:
				line_full = false
		if line_full:
			lines_filled.append(py + current_pos.y)
			for px in range(1, 11):
				set_cell(px, py + current_pos.y, -1)

func _calculate_score():
	var n: int = lines_filled.size()
	score += pow(4, n + 1) * sign(n)
	emit_signal("score_calculated", score)

func _take_tiles_down():
	for line in lines_filled:
		for py in range(line, 0, -1):
			for px in range(1, 11):
				set_cell(px, py, get_cell(px, py - 1))
	lines_filled.clear()

func _check_game_over():
	for px in range(1, 11):
		if get_cell(px, -1) != EMPTY_CELL:
			emit_signal("end_game")
			set_process(false)

func _erase_piece():
	for px in range(4):
		for py in range(4):
			if tetromino[current_piece][_rotate(px, py, current_rot)] == "X":
				set_cell(current_pos.x + px, current_pos.y + py, EMPTY_CELL)

func _draw_piece():
	for px in range(4):
		for py in range(4):
			if tetromino[current_piece][_rotate(px, py, current_rot)] == "X":
				set_cell(current_pos.x + px, current_pos.y + py, current_piece)
