extends CanvasLayer

var game_is_paused: bool = false

func toggle_visibility(status: bool):
	$Overlay.visible = status
	
func _process(_delta):
	if Input.is_action_just_pressed("Pause"):
		if game_is_paused:
			game_is_paused = false
			get_tree().paused = false
			self.toggle_visibility(false)
		else:
			game_is_paused = true
